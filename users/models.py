from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify 
# Create your models here.

class UserPost(models.Model):
	"""docstring for UserPost"""
	
	created =  models.DateTimeField(auto_now_add=True)
	username = models.ForeignKey(User, on_delete=models.CASCADE, default = User)
	primary_name = models.CharField(max_length = 200, name="primary_name", help_text="*Name/Nickname/personal brand", default="Guest")
	age = models.IntegerField(blank=True, name="age", null=True)
	secondary_name = models.CharField(max_length=100, name="secondary_name", help_text="if primary_name=nickname, then add real name", blank=True, null=True)
	background = models.CharField(max_length=200, name="background", verbose_name="paddler experience and background", help_text = "*One to three keywords to describe your own experience(Extreme WW padler/Expeditions/Competetive/guide/experienced/12yrs etc)")
	nationality = models.CharField(max_length=200, name="nationality", blank=True, null=True)
	image = models.ImageField(verbose_name="User image", name="user_image", upload_to="users/img", blank=True, null=True)
	social_media = models.URLField(help_text = "if you want to profile a social media account. Input path here", blank=True, null=True,)
	about_me = models.CharField(max_length=500, name="about_me", verbose_name="about me", blank=True, null=True)
	articles_written = models.CharField(max_length=200, verbose_name="article-url-paths", blank=True, null=True, help_text="This contains your articles. Dont touch it")

	class Meta:
		ordering = ["primary_name"]

	def __str__(self):
		return self.primary_name



		
