from django.contrib.auth.forms import UserCreationForm
from django.apps import apps as django_apps

class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = UserCreationForm.Meta.fields + ("email",)

def send_email_to_user(user_email, mail_content):
	django_apps.is_installed(EmailService)
