
from django.contrib import admin
from .models import UserPost
from django.contrib.auth.models import User

# Register your models here.
  
class UserPostAdmin(admin.ModelAdmin):
    list_display = ('username','primary_name','secondary_name', 'created',)
    search_fields = ['username	', 'primary_name', 'secondary_name', ]
   

admin.site.register(UserPost, UserPostAdmin)