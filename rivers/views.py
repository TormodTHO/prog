from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView 
from .models import RiverPost, User, RiverArticleSection
from users.models import UserPost


def get_river_list(request):
	riverdb = RiverPost.objects.all()
	context = {
	"riverdb": riverdb
	}
	return render(request, "riverlist.html", context)

def get_river_detail(request, slug):
    
    riversection = RiverPost.objects.get(slug = slug)
    authorbio = riversection.user_poster
    author = authorbio.primary_name
    authorbiblio = RiverPost.objects.filter(user_poster=authorbio)
    context = {
    'riversection': riversection,
    'authorbio': authorbio,
    'authorbiblio': authorbiblio,
 
    }
    return render(request, "riverdetails.html", context)

def get_section_detail(request, slug, section_slug):
    riversection = RiverPost.objects.get(slug = slug, section_slug=section_slug)
    authorbio = riversection.user_poster
    author = authorbio.primary_name
    authorbiblio = RiverPost.objects.filter(user_poster=authorbio)
    context = {
    'riversection': riversection,
    'authorbio': authorbio,
    'authorbiblio': authorbiblio,
 
    }
    return render(request, "riverdetails.html", context)

def get_article_detail(request, slug, section_slug, style_slug):
    riversection = RiverPost.objects.get(slug = slug, section_slug=section_slug, style_slug=style_slug)
    articlesection = RiverArticleSection.objects.filter(parent_article_id=riversection)
    authorbio = riversection.user_poster
    author = authorbio.primary_name
    authorbiblio = RiverPost.objects.filter(user_poster=authorbio)
    context = {
    'riversection': riversection,
    'articlesection': articlesection,
    'authorbio': authorbio,
    'authorbiblio': authorbiblio,
 
    }
    return render(request, "riverdetails.html", context)

