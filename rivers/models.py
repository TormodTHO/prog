from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from users.models import UserPost

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)
ARTICLE_STYLES = (
    (0,"Longform"),
    (1,"Shortform"),
    (2,"Extreme waterlevels"),
    (3, "Trip documentary"),
    (4, None),
)
RIVER_GRADE = (
    (0, "0 - Flatt vann"),
    (1, "1 - Easy: Fast moving water with riffles and small waves."),
    (2, "2 - Novice: Straightforward rapids with wide, clear channels."),
    (3, "3 - Hard: Fast current, complex water. Scouting is advisable for inexperienced parties"),
    (4, "4 - Very hard: Rapids may require moves above dangerous hazards. Scouting is necessary the first time down"),
    (5, "5 - Expert: Extremely long, obstructed, violent rapids which expose a paddler to endangerment."),
    (6, "6 - Extreme: The extremes of difficulty, unpredictability and danger. Occasionaly run"),
    )

REGION = (
    ('Troms og finnmark', 'Troms og finnmark'),
    ('Nordland', 'Nordland'),
    ('Trøndelag', 'Trøndelag'),
    ('Møre og Romsdal', 'Møre og Romsdal'),
    ('Vestland', 'Vestland'),
    ('Rogaland', 'Rogaland'),
    ('Agder', 'Agder'),
    ('Vestfold og telemark', 'Vestfold og telemark'),
    ('Viken', 'Viken'),
    ('Innlandet', 'Innlandet'),
    ('Oslo', 'Oslo'),
) 

class RiverPost(models.Model):
    river = models.CharField(max_length=200, unique=True, help_text="*River name")
    section = models.CharField(max_length=200, help_text="River section", blank=True, null=True)
    grade = models.IntegerField(choices=RIVER_GRADE, default=6, help_text="http://www.padling.no/elv/sikkerhet/gradering/")
    article_style=models.IntegerField(choices=ARTICLE_STYLES, default=1,help_text="*Describe the nature of your article.")
    update = models.CharField(max_length=400, help_text="Update field if river changes", blank=True)
    region = models.CharField(default='Oslo', choices=REGION, help_text="Municipality/region", max_length=200)
    nokken = models.URLField(blank = True, name="Nokken_path", help_text="If the river is on Nokken.net add a path here")
    main_image = models.ImageField(upload_to='upload/img', help_text="Header image")
    content = models.TextField(help_text="*Main content.", )
   
    rivermap = models.ImageField(upload_to='upload/map', blank=True)
    river_taggs =  models.TextField(help_text="Keyword information about the river(dangers, features, things to plan for, etc", blank=True)
    user_poster = models.ForeignKey(UserPost, on_delete=models.SET_NULL, null=True)
    """ User data """
    status = models.IntegerField(choices=STATUS, default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=200, unique=True, help_text="This becomes the URL tail", blank=True,null=True)
    section_slug=models.SlugField(max_length=200, help_text="This adds to the the URL tail if there is more than one section",blank=True,null=True)
    style_slug=models.SlugField(max_length=200, help_text="This adds the article style to the slugfield. Short-form articles are prioritized as page 1", blank=True,null=True)
    def __str__(self):
        return self.river

    class Meta:
        ordering = ['-created_on']


class RiverArticleSection(models.Model):
    section_header = models.CharField(max_length=200)
    parent_article = models.ForeignKey(RiverPost, on_delete=models.CASCADE, default=RiverPost)
    sub_image = models.ImageField(upload_to='upload/img', help_text="sub_image")
    content = models.TextField(help_text="*Main content.", )
    