from django.contrib import admin
from .models import RiverPost, RiverArticleSection
from django.contrib.auth.models import User

# Register your models here.
  
class RiverPostAdmin(admin.ModelAdmin):
    list_display = ('river', 'section', 'article_style', 'slug', 'status','created_on',)
    list_filter = ('status', 'river',)
    search_fields = ['river', 'content']
    prepopulated_fields = {
    'slug': ('river',),
    'section_slug': ('section',),
    'style_slug': ('article_style',),
    }
class RiverArticleSectionAdmin(admin.ModelAdmin):
	list_display = ('section_header','parent_article')
	list_filter = ('section_header','parent_article')
	search_fields = ('section_header','parent_article')

admin.site.register(RiverPost, RiverPostAdmin)
admin.site.register(RiverArticleSection, RiverArticleSectionAdmin)
