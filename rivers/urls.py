from django.urls import path
from rivers import views


urlpatterns = [
    path('',views.get_river_list, name='Riverlist'),
    path('<slug:slug>/', views.get_river_detail, name='Riverdetail'),
    path('<slug:slug>/<slug:section_slug>', views.get_section_detail, name='Riverdetail'),
    path('<slug:slug>/<slug:section_slug>/<slug:style_slug>', views.get_article_detail, name='Riverdetail'),

]